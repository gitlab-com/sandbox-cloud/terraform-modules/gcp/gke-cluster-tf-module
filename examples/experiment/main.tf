# [terraform-project]/main.tf

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.47"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.47"
    }
  }
  required_version = ">= 0.13"
}

# Define the Google Cloud Provider
provider "google" {
  credentials = file("./keys/gcp-service-account.json")
  project     = var.gcp_project
}

# Define the Google Cloud Provider with beta features
provider "google-beta" {
  credentials = file("./keys/gcp-service-account.json")
  project     = var.gcp_project
}

# Get the existing VPC network
data "google_compute_network" "vpc_network" {
  name = var.gcp_vpc_network
}

# Create VPC network for environment resources
resource "google_compute_network" "vpc_network" {
  auto_create_subnetworks = "false"
  description             = "VPC for experimental infrastructure"
  name                    = "experiment-vpc"
  routing_mode            = "REGIONAL"
}

# Create GCP Cloud Router for new VPC network
resource "google_compute_router" "router" {
  name    = "experiment-vpc-router"
  network = google_compute_network.vpc_network.name
  region  = var.gcp_region
}

# Create GCP NAT Gateway for new VPC network
resource "google_compute_router_nat" "nat_gateway" {
  name                               = "experiment-vpc-nat-gateway"
  nat_ip_allocate_option             = "AUTO_ONLY"
  router                             = google_compute_router.router.name
  region                             = var.gcp_region
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  # You can enable logging once monitoring infrastructure is ready
  log_config {
    enable = "false"
    filter = "ALL"
  }
}

# Provision a Kubernetes cluster
module "experiment_cluster" {
  source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module.git"

  cluster_name     = "tf-module-experiment-cluster"
  description      = "Cluster for Terraform module experiment"
  gcp_network      = google_compute_network.vpc_network.self_link
  gcp_project      = var.gcp_project
  gcp_region       = var.gcp_region
  gcp_region_zone  = var.gcp_region_zone
  gke_cluster_cidr = "10.140.0.0/22"

}
