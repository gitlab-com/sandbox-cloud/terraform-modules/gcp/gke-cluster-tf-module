# [terraform-project]/outputs.tf

# This will return a map with all of the outputs for the module
output "{{cool_purpose_cluster}}" {
  value = module.{{cool_purpose_cluster}}
}

# If you need a specific key as an output, you can use the dot notation shown above to access the map value.
output "{{cool_purpose_cluster}}_cluster_name" {
  value = module.{{cool_purpose_cluster}}.cluster.name
}
