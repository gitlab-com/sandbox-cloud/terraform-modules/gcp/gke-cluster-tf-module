# GKE Cluster Terraform Module

## Table of Contents

* [Prerequisites](#prerequisites)
* [Step-by-Step Instructions](#step-by-step-instructions)
* [Variables, Outputs, and Additional Customization](#variables-outputs-and-additional-customization)

## Prerequisites

1. Create a Git repository for your Terraform project, or use an existing repository with your Terraform configuration. **For security reasons, it is imperative that your [GitLab](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility) or [GitHub](https://docs.github.com/en/github/administering-a-repository/setting-repository-visibility) project is a `private` project (not `public` or `internal`) to avoid leaking your infrastructure credentials.**

1. Create a <a target="_blank" href="https://cloud.google.com/resource-manager/docs/creating-managing-projects">GCP project</a> or use an existing GCP project. You will need to decide which <a target="_blank" href="https://cloud.google.com/compute/docs/regions-zones">region and zone</a> you will use. The examples in this module use `us-east1` and `us-east1-c`.

1. In order to make requests against the GCP API, you need to authenticate to prove that it's you making the request. The preferred method of provisioning resources with Terraform is to use a GCP service account, which is a "robot account" that can be granted a limited set of IAM permissions. Use the [Terraform provider instructions](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started#adding-credentials) for creating a service account using the Google Cloud Console or `gcloud` CLI tool.

    > You can use the `GOOGLE_APPLICATION_CREDENTIALS` environment variable, however if you're working with multiple GCP projects you may find it easier to work use a key file in each of your repositories. In our examples, we use the `keys/gcp-service-account.json` file however you can name it whatever you would like (ex. my-project-name-a1b2c3d4.json).

    > It is important that you do not commit the service account `.json` file to your Git repository since this compromises your credentials. You should add the `/keys` directory to your `.gitignore` file. See the `.gitignore.example` file in this module for example configuration.

1. Create a <a target="_blank" href="https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network">VPC network</a>. It is likely that the <a target="_blank" href="https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_network">default VPC already exists</a>, however you cannot create subnets in a VPC that has `auto mode` enabled. You can learn more in the [GCP documentation](https://cloud.google.com/vpc/docs/vpc#auto-mode-considerations). If you have a custom VPC already, you can use the data source instead of declaring a new resource in your Terraform configuration.

1. Identify a `/22` (small cluster) or `/20` (medium cluster) CIDR network range in your VPC that has not already been allocated in your [GCP project's VPC network](https://console.cloud.google.com/networking/networks/list?authuser=0). In a production environment, you are responsible for determining a `/16` or `/20` (large cluster) CIDR range that aligns with your cloud administrator's network schema.

    > If you're using the `experiment` example, we will automatically allocate the `10.140.0.0/22` CIDR range in a custom VPC.

1. Determine the cluster name that uses alphanumeric characters and hyphens. (Example `cool-purpose-cluster`)

## Step-by-Step Instructions

**Reminder:** All double bracket `{{strings}}` should be replaced based on your desired configuration while preserving the hyphen or underscores syntax in each example.

1. Review the contents of `main.tf` in the appropriate [examples/ directory](examples/) to understand what is getting created. You can make changes as needed after you copy the example into your Terraform project.

1. Open your text editor and navigate to your new or existing Terraform project (also referred to as your "environment configuration").

1. Copy and paste the contents of `examples/{{example-name}}/main.tf` to the `main.tf` file in your Terraform project. You can add to the bottom of the file or create the file if it does not exist.

    > If the `provider "google" {}` and `provider "google-beta" {}` blocks already exist in your `main.tf` file, you do not need to add them twice.

1. Copy and paste the contents of `examples/{{example-name}}/outputs.tf` to the `outputs.tf` file in your Terraform project. You can add to the bottom of the file or create the file if it does not exist.

1. Copy and paste the contents of `examples/{{example-name}}/variables.tf` to the `variables.tf` file in your Terraform project. You can add to the bottom of the file or create the file if it does not exist.

1. Copy and paste the `examples/{{example-name}}/.gitignore.example` file to your Terraform project. If you do not have a `.gitignore` file, you can simply remove the `.example` extension. If you have a `.gitignore` file, take a few moments to add the lines from `.gitignore.example` to your existing file.

    > It is your discretion whether your `.tfvars` files are committed to your source code, however we have included them in the `.gitignore` as a security precaution.

1. Copy and paste the contents of `examples/{{example-name}}/terraform.tfvars.json` to the `terraform.tfvars.json` file in your Terraform project.

    > If you have an existing `terraform.tfvars` file or another `.tfvars` file, you can create the key/value pairs in your existing file using the appropriate syntax.

1. Update the values in `terraform.tfvars.json` to match your environment configuration. You can see a description of each of the variables in the `variables.tf` file.

    > The GCP project can be either the slug of the project or the 12-digit project ID. See the GCP documentation for [identifying projects](https://cloud.google.com/resource-manager/docs/creating-managing-projects#identifying_projects) to learn more.

    ```
    {
        "gcp_dns_zone_name": "my-dns-zone-name",
        "gcp_project": "my-project-name-a1b2c3d4",
        "gcp_region": "us-east1",
        "gcp_region_zone": "us-east1-c"
    }
    ```

1. Create a new file named `gcp-service-account.json` in the `keys/` directory with your service account credentials JSON keyfile.

    ```
    {
      "type": "service_account",
      "project_id": "demosys-saas",
      "private_key_id": "NULL",
      "private_key": "-----BEGIN PRIVATE KEY-----\nNULL\n-----END PRIVATE KEY-----\n",
      "client_email": "NULL-compute@developer.gserviceaccount.com",
      "client_id": "NULL",
      "auth_uri": "https://accounts.google.com/o/oauth2/auth",
      "token_uri": "https://oauth2.googleapis.com/token",
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
      "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/NULL-compute%40developer.gserviceaccount.com"
    }
    ```

    > If you are using `gcloud` or `GOOGLE_APPLICATION_CREDENTIALS` environment variable, comment out or remove `credentials = file("./keys/gcp-service-account.json")` in the `provider "google" {}` and `provider "google-beta" {}` blocks in `main.tf`.

    ```
    # [terraform-project]/main.tf

    # Define the Google Cloud Provider
    provider "google" {
      # credentials = file("./keys/gcp-service-account.json")
      project     = var.gcp_project
    }

    # Define the Google Cloud Provider with beta features
    provider "google-beta" {
      # credentials = file("./keys/gcp-service-account.json")
      project     = var.gcp_project
    }
    ```

1. Open your Terminal and navigate to the directory where your Terraform environment configuration resides.

    ```
    cd ~/Sites/terraform-project
    ```

1. Run `terraform init` to initialize your Terraform configuration and create a Terraform state file.

    ```
    terraform init
    ```

    > If there are any problems with authenticating with the Google API, you will see the errors during this step. These are generic Terraform errors and are not specific to this module usually. You should be able to perform a Google search to get help with the error message that you're seeing.

    ```
    Initializing modules...
    Downloading git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module.git for experiment_instance...
    - experiment_instance in .terraform/modules/experiment_cluster

    Initializing the backend...

    Initializing provider plugins...
    - Finding hashicorp/google-beta versions matching ">= 3.47.0"...
    - Finding hashicorp/google versions matching ">= 3.47.0"...
    - Installing hashicorp/google-beta v3.64.0...
    - Installed hashicorp/google-beta v3.64.0 (signed by HashiCorp)
    - Installing hashicorp/google v3.64.0...
    - Installed hashicorp/google v3.64.0 (signed by HashiCorp)

    Terraform has created a lock file .terraform.lock.hcl to record the provider
    selections it made above. Include this file in your version control repository
    so that Terraform can guarantee to make the same selections by default when
    you run "terraform init" in the future.

    Terraform has been successfully initialized!

    You may now begin working with Terraform. Try running "terraform plan" to see
    any changes that are required for your infrastructure. All Terraform commands
    should now work.

    If you ever set or change modules or backend configuration for Terraform,
    rerun this command to reinitialize your working directory. If you forget, other
    commands will detect it and remind you to do so if necessary.
    ```

1. Run `terraform plan` to see how your Terraform configuration will make changes to your infrastructure.

    ```
    terraform plan
    ```

    ```
    ❯ terraform plan

    Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
      + create

    Terraform will perform the following actions:

      # module.experiment_cluster.google_compute_subnetwork.subnet will be created
      + resource "google_compute_subnetwork" "subnet" {
          # ...
        }

      # module.experiment_cluster.google_container_cluster.cluster will be created
      + resource "google_container_cluster" "cluster" {
          # ...
        }

      # module.experiment_cluster.google_container_node_pool.node_pool will be created
      + resource "google_container_node_pool" "node_pool" {
          #
        }

    Plan: 3 to add, 0 to change, 0 to destroy.

    Changes to Outputs:
      + experiment_cluster      = {
          + cluster    = {
              + autoscaling_profile    = "BALANCED"
              + cidr                   = "10.142.4.0/22"
              + description            = "Cluster for Terraform module experiment"
              + endpoint               = (known after apply)
              + id                     = (known after apply)
              + instance_group_urls    = (known after apply)
              + location               = "us-east1-c"
              + maintenance_start_time = "03:00"
              + master_version         = (known after apply)
              + name                   = "tf-module-experiment-cluster"
              + release_channel        = "REGULAR"
              + resource_labels        = null
              + self_link              = (known after apply)
              + type                   = "zonal"
            }
          + gcp        = {
              + machine_type      = "e2-standard-2"
              + network           = "https://www.googleapis.com/compute/v1/projects/demosys-sandbox/global/networks/default"
              + preemptible_nodes = true
              + project           = "demosys-sandbox"
              + region            = "us-east1"
              + region_zone       = "us-east1-c"
            }
          + gke        = {
              + autoscaling_profile    = "BALANCED"
              + cluster_type           = "zonal"
              + maintenance_start_time = "03:00"
              + node_count_max         = 32
              + node_count_min         = 1
              + node_pods_count_max    = 32
              + release_channel        = "REGULAR"
              + version_prefix         = "1.18."
            }
          + labels     = {}
          + node_pool  = {
              + id                  = (known after apply)
              + instance_group_urls = (known after apply)
              + name                = "tf-module-experiment-cluster-node-pool"
              + node_count_max      = 32
              + node_count_min      = 1
              + node_pods_count_max = 32
              + resource_labels     = null
            }
          + subnetwork = {
              + cluster_cidr = "10.142.4.0/22"
              + ranges       = {
                  + nodes    = {
                      + cidr = "10.142.4.0/26"
                      + name = "tf-module-experiment-cluster-subnet-nodes"
                    }
                  + pods     = {
                      + cidr = "10.142.6.0/23"
                      + name = "tf-module-experiment-cluster-subnet-pods"
                    }
                  + services = {
                      + cidr = "10.142.5.0/24"
                      + name = "tf-module-experiment-cluster-subnet-services"
                    }
                }
              + self_link    = (known after apply)
            }
        }
      + experiment_cluster_name = "tf-module-experiment-cluster"
    ```

1. Run `terraform apply` to deploy the resources that were shown in the plan output. If you are using an existing Terraform project, it is strongly recommended to use targeting with `terraform apply -target=module.{{my_instance_name}}` to only make changes to the specific module(s) or resource(s) that you're working with.

    ```
    terraform apply -target=module.experiment_cluster
    ```

    > It can take up to 30 minutes for a cluster deployment to be completed. It may take an additional few minutes for Kubernetes services to be available after the Terraform deployment has completed.

    ```
    Do you want to perform these actions?
      Terraform will perform the actions described above.
      Only 'yes' will be accepted to approve.

      Enter a value: yes

    google_compute_network.vpc_network: Creating...
    google_compute_network.vpc_network: Still creating... [10s elapsed]
    google_compute_network.vpc_network: Creation complete after 22s [id=projects/demosys-sandbox/global/networks/experiment-vpc]
    google_compute_router.router: Creating...
    module.experiment_cluster.google_compute_subnetwork.subnet: Creating...
    module.experiment_cluster.google_compute_subnetwork.subnet: Still creating... [10s elapsed]
    google_compute_router.router: Still creating... [10s elapsed]
    google_compute_router.router: Creation complete after 12s [id=projects/demosys-sandbox/regions/us-east1/routers/experiment-vpc-router]
    google_compute_router_nat.nat_gateway: Creating...
    module.experiment_cluster.google_compute_subnetwork.subnet: Still creating... [20s elapsed]
    google_compute_router_nat.nat_gateway: Still creating... [10s elapsed]
    module.experiment_cluster.google_compute_subnetwork.subnet: Still creating... [30s elapsed]
    google_compute_router_nat.nat_gateway: Still creating... [20s elapsed]
    module.experiment_cluster.google_compute_subnetwork.subnet: Creation complete after 34s [id=projects/demosys-sandbox/regions/us-east1/subnetworks/tf-module-experiment-cluster-subnet-nodes]
    module.experiment_cluster.google_container_cluster.cluster: Creating...
    google_compute_router_nat.nat_gateway: Creation complete after 23s [id=demosys-sandbox/us-east1/experiment-vpc-router/experiment-vpc-nat-gateway]
    module.experiment_cluster.google_container_cluster.cluster: Still creating... [10s elapsed]
    # ...
    module.experiment_cluster.google_container_cluster.cluster: Still creating... [5m40s elapsed]
    module.experiment_cluster.google_container_cluster.cluster: Creation complete after 5m41s [id=projects/demosys-sandbox/locations/us-east1-c/clusters/tf-module-experiment-cluster]
    module.experiment_cluster.google_container_node_pool.node_pool: Creating...
    module.experiment_cluster.google_container_node_pool.node_pool: Still creating... [10s elapsed]
    # ...
    module.experiment_cluster.google_container_node_pool.node_pool: Still creating... [5m50s elapsed]
    module.experiment_cluster.google_container_node_pool.node_pool: Creation complete after 5m59s [id=projects/demosys-sandbox/locations/us-east1-c/clusters/tf-module-experiment-cluster/nodePools/tf-module-experiment-cluster-node-pool]

    Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
    ```

1. If this was an experiment and you're ready to clean up your work, run `terraform destroy -target=module.{{my_cluster_name}}` to destroy the resources that were created. If you are using an existing Terraform project, it is imperative that you use targeting with `terraform destroy` to only make changes to the specific module(s) or resource(s) that you're working with. **If you do not specify a target, all of your Terraform-managed infrastructure will be permanently destroyed and data loss will occur.**

    ```
    terraform destroy -target=module.{{my_cluster_name}}
    ```

## Variables, Outputs, and Additional Customization

See the [README.md](README.md) for more information on [variables](README.md#variables) and [outputs](README.md#outputs). You can review the [main.tf](main.tf) file to see all of the resources that are being created with this module, and the respective variables that can be configured to customize them.
