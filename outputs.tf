###############################################################################
# GKE Cluster Configuration Module
# -----------------------------------------------------------------------------
# outputs.tf
###############################################################################

output "cluster" {
  value = {
    autoscaling_profile    = var.gke_autoscaling_profile
    cidr                   = var.gke_cluster_cidr
    description            = var.description
    endpoint               = google_container_cluster.cluster.endpoint
    id                     = google_container_cluster.cluster.id
    location               = google_container_cluster.cluster.location
    maintenance_start_time = var.gke_maintenance_start_time
    master_version         = google_container_cluster.cluster.master_version
    name                   = google_container_cluster.cluster.name
    release_channel        = var.gke_release_channel
    resource_labels        = google_container_cluster.cluster.resource_labels
    self_link              = google_container_cluster.cluster.self_link
    tags                   = google_container_node_pool.node_pool.node_config[*].tags
    type                   = var.gke_cluster_type
  }
}

output "gcp" {
  value = {
    machine_type      = var.gcp_machine_type
    network           = var.gcp_network
    preemptible_nodes = var.gcp_preemptible_nodes
    project           = var.gcp_project
    region            = var.gcp_region
    region_zone       = var.gcp_region_zone
  }
}

output "gke" {
  value = {
    autoscaling_profile    = var.gke_autoscaling_profile
    cluster_type           = var.gke_cluster_type
    node_count_max         = var.gke_node_count_max
    node_count_min         = var.gke_node_count_min
    node_pods_count_max    = var.gke_node_pods_count_max
    maintenance_start_time = var.gke_maintenance_start_time
    release_channel        = var.gke_release_channel
    version_prefix         = var.gke_version_prefix
  }
}

output "labels" {
  value = var.labels
}

output "node_pool" {
  value = {
    id                  = google_container_node_pool.node_pool.id
    instance_group_urls = google_container_node_pool.node_pool.instance_group_urls
    node_count_min      = var.gke_node_count_min
    node_count_max      = var.gke_node_count_max
    node_pods_count_max = var.gke_node_pods_count_max
    name                = google_container_node_pool.node_pool.name
    resource_labels     = google_container_cluster.cluster.resource_labels
  }
}

output "subnetwork" {
  value = {
    cluster_cidr = var.gke_cluster_cidr
    ranges = {
      nodes = {
        name = google_compute_subnetwork.subnet.name
        cidr = google_compute_subnetwork.subnet.ip_cidr_range
      },
      pods = {
        name = google_compute_subnetwork.subnet.secondary_ip_range[1].range_name
        cidr = google_compute_subnetwork.subnet.secondary_ip_range[1].ip_cidr_range
      },
      services = {
        name = google_compute_subnetwork.subnet.secondary_ip_range[0].range_name
        cidr = google_compute_subnetwork.subnet.secondary_ip_range[0].ip_cidr_range
      },
    }
    self_link = google_compute_subnetwork.subnet.self_link
  }
}
