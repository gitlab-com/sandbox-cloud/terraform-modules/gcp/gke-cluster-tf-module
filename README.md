# GKE Cluster Terraform Module

This Terraform module is designed to be used for creating a Google Cloud Platform (GCP) managed Kubernetes cluster with Google Kubernetes Engine (GKE). The size of the cluster can be customized to accomodate right-sized for demo, sandbox, and testing use cases up to large-scale production services.

## Table of Contents

* [Version Compatibility](#version-compatibility)
* [Usage Instructions](#usage-instructions)
    * [Cluster Sizing and Best Practices](SIZING.md)
        * [Cost Calculations](SIZING.md#cost-calculations)
        * [Small Cluster (/22 CIDR)](SIZING.md#small-cluster-22-cidr)
        * [Medium Cluster (/20 CIDR)](SIZING.md#small-cluster-20-cidr)
        * [Large Cluster (/16 CIDR)](SIZING.md#small-cluster-16-cidr)
    * [Prerequisites](INSTALL.md#prerequisites)
    * [Step-by-Step Instructions](INSTALL.md#step-by-step-instructions)
    * [Variables, Outputs, and Additional Customization](INSTALL.md#variables-outputs-and-additional-customization)
    * [Experiment Example](#experiment-example)
    * [Placeholders Example](#placeholders-example)
    <!--* [GitLab Demo Systems Example](#gitlab-demo-systems-example)-->
    <!--* [GitLab Sandbox Cloud Example](#gitlab-sandbox-cloud-example)-->
* [Variables](#variables)
* [Outputs](#outputs)
* [Authors and Maintainers](#authors-and-maintainers)

### Version Compatibility

* Minimum Terraform v0.13
* Tested with Terraform v0.13, v0.14, v0.15
* See the [hashicorp/terraform CHANGELOG](https://github.com/hashicorp/terraform/blob/master/CHANGELOG.md) for future breaking and deprecation changes

## Usage Instructions

See [SIZING.md](SIZING.md) for cluster sizing and best practices for using this Terraform module.

See [INSTALL.md](INSTALL.md) for step-by-step instructions for using this Terraform module.

### Experiment Example

This example includes a basic configuration to get you started with using this module for the first time or a proof-of-concept use case.
* [examples/experiment/main.tf](examples/experiment/main.tf)
* [examples/experiment/outputs.tf](examples/experiment/outputs.tf)
* [examples/experiment/terraform.tfvars.json](examples/experiment/terraform.tfvars.json)
* [examples/experiment/variables.tf](examples/experiment/variables.tf)

### Placeholders Example

This example includes bracket placeholders for you to easily copy and paste into your own environment configuration and start using this module for any use case.
* [examples/placeholders/main.tf](examples/placeholders/main.tf)
* [examples/placeholders/outputs.tf](examples/placeholders/outputs.tf)
* [examples/placeholders/terraform.tfvars.json](examples/placeholders/terraform.tfvars.json)
* [examples/placeholders/variables.tf](examples/placeholders/variables.tf)

### Variables

We use top-level variables where possible instead of maps to allow easier handling of default values with partially defined maps, and reduce complexity for developers who are just getting started with Terraform syntax.

<table>
<thead>
<tr>
    <th style="width: 25%;">Variable Key / Output Key</th>
    <th style="width: 40%;">Description</th>
    <th style="width: 10%;">Required</th>
    <th style="width: 25%;">Example Value</th>
</tr>
</thead>
<tbody>
<tr>
    <td>
        <code>cluster_name</code>
    </td>
    <td>Cluster short name. Only alphanumeric characters and hyphens are allowed.</a></td>
    <td><strong>Yes</strong></td>
    <td><code>cool-product-cluster</code></td>
</tr>
<tr>
    <td>
        <code>description</code>
    </td>
    <td>Cluster description</a></td>
    <td><strong>Yes</strong></td>
    <td><code>My cluster for cool product containers</code></td>
</tr>
<tr>
    <td>
        <code>gcp_machine_type</code>
    </td>
    <td>The GCP machine type to use for cluster nodes. See [Cluster Sizing and Best Practices](#cluster-sizing-and-best-practices) for instructions on custom cluster sizing. In most cases, you should leave this at the default value.</a></td>
    <td>No</td>
    <td><code>e2-standard-2</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>gcp_network</code>
    </td>
    <td>The object or self link for the network created in the parent module. You must use a custom VPC or a VPC without [auto mode](https://cloud.google.com/vpc/docs/vpc#auto-mode-considerations) enabled.</a></td>
    <td><strong>Yes</strong></td>
    <td><code>google_compute_network.sandbox_vpc.self_link</code></td>
</tr>
<tr>
    <td>
        <code>gcp_preemptible_nodes</code>
    </td>
    <td><a target="_blank" href="https://cloud.google.com/compute/docs/instances/preemptible#preemption_process">Preemptible VMs</a> are Compute Engine VM instances that last a maximum of 24 hours and provide no availability guarantees. You can use preemptible VMs in your GKE clusters or node pools to run batch or fault-tolerant jobs that are less sensitive to the ephemeral, non-guaranteed nature of preemptible VMs. We set this to true for sandbox use cases for cost savings, and false for production use cases for reliability.</a></td>
    <td>No</td>
    <td>
        <code>true</code> <small>(default)</small><br />
        <code>false</code>
    </td>
</tr>
<tr>
    <td>
        <code>gcp_project</code>
    </td>
    <td>The GCP project ID (may be an alphanumeric slug) that the resources are deployed in.</a></td>
    <td><strong>Yes</strong></td>
    <td><code>my-project-name</code></td>
</tr>
<tr>
    <td>
        <code>gcp_region</code>
    </td>
    <td>The GCP region name for this environment.</a></td>
    <td><strong>Yes</strong></td>
    <td><code>us-east1</code></td>
</tr>
<tr>
    <td>
        <code>gcp_region_zone</code>
    </td>
    <td>The GCP region availability zone for this environment. This must match the gcp_region.</a></td>
    <td><strong>Yes</strong></td>
    <td><code>us-east1-c</code></td>
</tr>
<tr>
    <td>
        <code>gke_autoscaling_profile</code>
    </td>
    <td>The autoscaling profile for cluster nodes</a></td>
    <td>No</td>
    <td>
        <code>BALANCED</code> <small>(default)</small><br />
        <code>OPTIMIZE_UTILIZATION
    </td>
</tr>
<tr>
    <td>
        <code>gke_cluster_cidr</code>
    </td>
    <td>The network /22 CIDR range for this cluster</a></td>
    <td><strong>Yes</strong></td>
    <td><code>10.120.0.0/22</code></td>
</tr>
<tr>
    <td>
        <code>gke_cluster_type</code>
    </td>
    <td>If gcp_cluster_type is <code>regional</code>, the cluster will be across 3 zones in the region. This is triple redundancy that costs 3x more than a zonal cluster. If gcp_cluster_type is <code>zonal</code>, the cluster will be created with 1 zone and is more cost efficient for sandbox purposes.</a></td>
    <td>No</td>
    <td>
        <code>zonal</code> <small>(default)</small><br />
        <code>regional</code>
    </td>
</tr>
<tr>
    <td>
        <code>gke_cluster_tags</code>
    </td>
    <td>The network tags to apply to the GKE cluster.</td>
    <td>No</td>
    <td>
        <code>[]</code> <small>(default)</small><br/>
        <code>["allow-health-check", "allow-https"]</code>
    </td>
</tr>
<tr>
    <td>
        <code>gke_node_count_max</code>
    </td>
    <td>The maximum number of nodes in the autoscaling node pool that should be running at any given time. This must be greater than 1. See [Cluster Sizing and Best Practices](#cluster-sizing-and-best-practices) for instructions on custom cluster sizing. In most cases, you should leave this at the default value. If gke_cluster_type is <code>regional</code>, the max_node_count should be divided by 3 and rounded down to the nearest whole number. This accomodates for the number of nodes running in each of the 3 availability zones.</a></td>
    <td>No</td>
    <td><code>16</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>gke_node_count_min</code>
    </td>
    <td>The minimum number of nodes in the autoscaling node pool that should be running at any given time. This must be greater than 0 and less than local.max_node_count. If gcp_cluster_type is <code>regional</code>, a value of <code>1</code> will be interpreted as <code>3</code> (one node in 3 separate availability zones).</a></td>
    <td>No</td>
    <td><code>1</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>gke_node_pods_count_max</code>
    </td>
    <td>The maximum number of pods per node. With a maximum of 16 nodes in a sandbox sized cluster, this allows up to 16 pods per node. See [Cluster Sizing and Best Practices](#cluster-sizing-and-best-practices) for instructions on custom cluster sizing. In most cases, you should leave this at the default value.</a></td>
    <td>No</td>
    <td><code>16</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>gke_maintenance_start_time</code>
    </td>
    <td>The start time of maintenance windows for Kubernetes operations and upgrades in HH:MM format (GMT) timezone.</a></td>
    <td>No</td>
    <td><code>03:00</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>gke_release_channel</code>
    </td>
    <td>The <a target="_blank" href="https://cloud.google.com/kubernetes-engine/docs/concepts/release-channels">release channel</a> for the cluster.</a></td>
    <td>No</td>
    <td>
        <code>RAPID</code><br />
        <code>REGULAR</code> <small>(default)</small><br />
        <code>STABLE</code><br />
        <code>UNSPECIFIED</code><br />
    </td>
</tr>
<tr>
    <td>
        <code>gke_version_prefix</code>
    </td>
    <td>The version number for the Kubernetes master and nodes. This is fuzz tested using the google_container_engine_versions data source to get the appropriate version number.</a></td>
    <td>No</td>
    <td><code>1.17.</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>labels</code>
    </td>
    <td>Labels to place on the cluster and node pool resources.</a></td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>"label_key1" = "label-value1"</code><br />
        <code>"label_key2" = "label-value2"</code><br />
        <code>}</code><br />
    </td>
</tr>
<tr>
    <td>
        <code>workload_identity_enabled</code>
    </td>
    <td>Enables or Disables Google Workload Identity on the cluster.</a></td>
    <td>No</td>
    <td>
        <code>true</code><br />
        <code>false</code> <small>(default)</small>
    </td>
</tr>
</tbody>
</table>

### Outputs

The outputs are returned as a map with sub arrays that use dot notation. You can see how each output is defined in [outputs.tf](outputs.tf).

```hcl
# Get a map with all values for the module
module.{{cool_product_cluster}}

# Get individual values
module.{{cool_product_cluster}}.cluster.autoscaling_profile
module.{{cool_product_cluster}}.cluster.cidr
module.{{cool_product_cluster}}.cluster.description
module.{{cool_product_cluster}}.cluster.endpoint
module.{{cool_product_cluster}}.cluster.id
module.{{cool_product_cluster}}.cluster.instance_group_urls
module.{{cool_product_cluster}}.cluster.location
module.{{cool_product_cluster}}.cluster.maintenance_start_time
module.{{cool_product_cluster}}.cluster.master_version
module.{{cool_product_cluster}}.cluster.name
module.{{cool_product_cluster}}.cluster.release_channel
module.{{cool_product_cluster}}.cluster.resource_labels
module.{{cool_product_cluster}}.cluster.self_link
module.{{cool_product_cluster}}.cluster.tags
module.{{cool_product_cluster}}.cluster.type
module.{{cool_product_cluster}}.gcp.machine_type
module.{{cool_product_cluster}}.gcp.network
module.{{cool_product_cluster}}.gcp.preemptible_nodes
module.{{cool_product_cluster}}.gcp.project
module.{{cool_product_cluster}}.gcp.region
module.{{cool_product_cluster}}.gcp.region_zone
module.{{cool_product_cluster}}.gke.autoscaling_profile
module.{{cool_product_cluster}}.gke.cluster_type
module.{{cool_product_cluster}}.gke.node_count_max
module.{{cool_product_cluster}}.gke.node_count_min
module.{{cool_product_cluster}}.gke.node_pods_count_max
module.{{cool_product_cluster}}.gke.maintenance_start_time
module.{{cool_product_cluster}}.gke.release_channel
module.{{cool_product_cluster}}.gke.version_prefix
module.{{cool_product_cluster}}.labels
module.{{cool_product_cluster}}.labels.label_key1
module.{{cool_product_cluster}}.labels.label_key2
module.{{cool_product_cluster}}.node_pool.id
module.{{cool_product_cluster}}.node_pool.instance_group_urls
module.{{cool_product_cluster}}.node_pool.node_count_min
module.{{cool_product_cluster}}.node_pool.node_count_max
module.{{cool_product_cluster}}.node_pool.node_pods_count_max
module.{{cool_product_cluster}}.node_pool.name
module.{{cool_product_cluster}}.node_pool.resource_labels
module.{{cool_product_cluster}}.subnetwork.cluster_cidr
module.{{cool_product_cluster}}.subnetwork.ranges.nodes.name
module.{{cool_product_cluster}}.subnetwork.ranges.nodes.cidr
module.{{cool_product_cluster}}.subnetwork.ranges.pods.name
module.{{cool_product_cluster}}.subnetwork.ranges.pods.cidr
module.{{cool_product_cluster}}.subnetwork.ranges.services.name
module.{{cool_product_cluster}}.subnetwork.ranges.services.cidr
module.{{cool_product_cluster}}.subnetwork.self_link
```

### Authors and Maintainers

* Jeff Martin / @jeffersonmartin / jmartin@gitlab.com
